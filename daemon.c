/*
 * Copyright (C) 2023 Affe Null
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <errno.h>
#include <fcntl.h>
#include <linux/limits.h>
#include <linux/rtc.h>
#include <limits.h>
#include <pwd.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/inotify.h>
#include <sys/ioctl.h>
#include <sys/timerfd.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include "libalarmwakeup.h"

#define EV_BUF_SIZE 2

enum alarm_status {
	NO_ALARM,
	ALARM_NEXT,
	ALARM_REPEAT,
};

static bool debug_log_enabled = false;

static void dbgmsg(const char *fmt, ...)
{
	va_list ap;

	if (!debug_log_enabled)
		return;

	va_start(ap, fmt);
	vprintf(fmt, ap);
	va_end(ap);
}

static enum alarm_status find_next(struct alarmwakeup_file *aw,
				   struct alarmwakeup_buf *alarm)
{
	time_t t = time(NULL);
	struct tm *lt = localtime(&t);
	int hour = lt->tm_hour, minute = lt->tm_min;

	while (alarmwakeup_read(aw, alarm)) {
		dbgmsg("There is an alarm at %02d:%02d\n", alarm->hour, alarm->minute);
		if (alarm->hour > hour || (alarm->hour == hour && alarm->minute >= minute))
			return ALARM_NEXT;
	}

	dbgmsg("Rewinding...\n");

	alarmwakeup_reopen(aw);

	return alarmwakeup_read(aw, alarm) ? ALARM_REPEAT : NO_ALARM;
}

static bool set_rtc(int rtc, int hour, int minute)
{
	struct rtc_time rt, alarm = { 0 };
	time_t rtc_time, rtc_alarm_time, alarm_time, current_time;
	struct tm lt;

	current_time = time(NULL);
	lt = *localtime(&current_time);
	lt.tm_hour = hour;
	lt.tm_min = minute;
	lt.tm_sec = 0;
	alarm_time = mktime(&lt);
	if (alarm_time < current_time) {
		lt.tm_isdst = -1;
		lt.tm_mday += 1;
		alarm_time = mktime(&lt);
	}

	ioctl(rtc, RTC_RD_TIME, &rt);
	dbgmsg("Current RTC time is %02d:%02d:%02d\n",
	       rt.tm_hour, rt.tm_min, rt.tm_sec);
	rtc_time = rt.tm_hour * 3600 + rt.tm_min * 60 + rt.tm_sec;
	rtc_alarm_time = ((24 * 60 * 60) + rtc_time + alarm_time - current_time);
	rtc_alarm_time %= (24 * 60 * 60);

	/*
	 * If we're within one minute past the alarm time or up to one
	 * second before it, do not set the RTC, just trigger immediately
	 */
	if (rtc_time >= (rtc_alarm_time - 1) &&
	    rtc_time < (rtc_alarm_time + 60)) {
		return false;
	}

	alarm.tm_hour = rtc_alarm_time / 3600;
	alarm.tm_min = rtc_alarm_time / 60 % 60;
	alarm.tm_sec = rtc_alarm_time % 60;
	dbgmsg("The alarm is %02d:%02d:%02d in RTC time\n",
	       alarm.tm_hour, alarm.tm_min, alarm.tm_sec);

	if (ioctl(rtc, RTC_ALM_SET, &alarm))
		perror("Cannot set RTC alarm");

	if (ioctl(rtc, RTC_AIE_ON))
		perror("Cannot enable RTC alarm");

	return true;
}

static void trigger_alarm(struct alarmwakeup_buf *alarm)
{
	pid_t child;

	dbgmsg("< < < < 🔔 > > > >\n  %s\n", alarm->label);
	child = fork();

	if (child == -1) {
		perror("fork");
		return;
	}

	if (child == 0) {
		execl("/bin/sh", "sh", "-c", alarm->command, (char*)NULL);
		perror("/bin/sh");
		_exit(127);
	}

	waitpid(child, NULL, 0);
}

static int add_watch(int infd)
{
	char *home = getpwuid(getuid())->pw_dir;
	char *path = alloca(strlen(home) + strlen("/.config") + 1);

	sprintf(path, "%s/.config", home);
	return inotify_add_watch(infd, path, IN_MOVED_TO);
}

static void run_daemon(int rtc, struct alarmwakeup_file *aw)
{
	struct epoll_event ev, events[EV_BUF_SIZE];
	struct alarmwakeup_buf alarm;
	int epollfd, timerfd, infd, watch, i, nfds;
	bool reloaded = false, skip_trigger = false;
	struct itimerspec timerspec = {
		.it_value.tv_sec = LLONG_MAX
	};

	epollfd = epoll_create1(EPOLL_CLOEXEC);
	if (epollfd < 0) {
		perror("epoll_create1");
		return;
	}
	infd = inotify_init1(IN_CLOEXEC);
	if (infd < 0) {
		perror("inotify_init1");
		return;
	}
	timerfd = timerfd_create(CLOCK_REALTIME, TFD_CLOEXEC);
	if (timerfd < 0) {
		perror("timerfd_create");
		return;
	}

	watch = add_watch(infd);
	if (watch < 0) {
		perror("inotify_add_watch");
		return;
	}

	ev.events = EPOLLIN | EPOLLWAKEUP;
	ev.data.fd = rtc;
	if (epoll_ctl(epollfd, EPOLL_CTL_ADD, rtc, &ev)) {
		perror("epoll_ctl(rtc)");
		return;
	}

	ev.events = EPOLLIN;
	ev.data.fd = infd;
	if (epoll_ctl(epollfd, EPOLL_CTL_ADD, infd, &ev)) {
		perror("epoll_ctl(inotify)");
		return;
	}

	ev.events = EPOLLIN;
	ev.data.fd = timerfd;
	if (epoll_ctl(epollfd, EPOLL_CTL_ADD, timerfd, &ev)) {
		perror("epoll_ctl(timerfd)");
		return;
	}

	if (timerfd_settime(timerfd,
			    TFD_TIMER_ABSTIME | TFD_TIMER_CANCEL_ON_SET,
			    &timerspec,
			    NULL) < 0)
	{
		perror("timerfd_settime");
		return;
	}

	if (setuid(getuid())) {
		perror("Failed to drop privileges");
		return;
	}

	alarmwakeup_reopen(aw);

	while (true) {
		enum alarm_status status = find_next(aw, &alarm);

		if (status != NO_ALARM) {
			if (status == ALARM_REPEAT)
				reloaded = true;
			dbgmsg("Next alarm at %02d:%02d [%s]: %s\n",
			       alarm.hour,
			       alarm.minute,
			       alarm.label,
			       alarm.command);
			if (!set_rtc(rtc, alarm.hour, alarm.minute)) {
				if (reloaded) {
					dbgmsg("Waiting for time to change\n");
					set_rtc(rtc, alarm.hour, alarm.minute+2);
					skip_trigger = true;
				} else {
					dbgmsg("Triggering immediately\n");
					trigger_alarm(&alarm);
					continue;
				}
			} else {
				skip_trigger = false;
			}
		} else {
			dbgmsg("No alarms\n");

			if (ioctl(rtc, RTC_AIE_OFF))
				perror("Cannot disable RTC alarm");
		}

		reloaded = false;

		do {
			nfds = epoll_wait(epollfd, events, EV_BUF_SIZE, -1);
		} while (nfds <= 0 && errno == EINTR);
		if (nfds < 0) {
			perror("epoll_wait");
			return;
		}
		for (i = 0; i < nfds; i++) {
			if (events[i].data.fd == rtc) {
				long tmp;
				if (read(rtc, &tmp, sizeof(long)) > 0) {
					dbgmsg("rtc wakeup received\n");
					if (!skip_trigger)
						trigger_alarm(&alarm);
					else
						skip_trigger = false;
				} else {
					perror("read(rtc)");
				}
			} else if (events[i].data.fd == infd) {
				struct {
					struct inotify_event ev;
					char name[NAME_MAX + 1];
				} inev;
				if (read(infd, &inev, sizeof(inev)) > 0) {
					if (inev.ev.mask & IN_MOVED_TO &&
					    0 == strcmp(inev.name, "alarmwakeup")) {
						dbgmsg("Reloading...\n");
						alarmwakeup_reopen(aw);
						reloaded = true;
					}
				} else {
					perror("read(inotify)");
				}
			} else if (events[i].data.fd == timerfd) {
				uint64_t time;
				/* ignore errors, ECANCELED is likely */
				read(timerfd, &time, sizeof(time));
				dbgmsg("Time changed, resetting alarm...\n");
				alarmwakeup_reopen(aw);
				/*
				 * we DO want alarms to trigger, so we
				 * don't set reloaded = true here
				 */
			}
		}
	}
}

int main(int argc, char * const *argv)
{
	struct alarmwakeup_file *aw;
	int rtc;

	if (getenv("ALARMWAKEUP_DEBUG"))
		debug_log_enabled = true;

	if (argc < 2 || argv[1][0] != '-' || argv[1][1] != 'f') {
		if (fork() > 0) {
			_exit(0);
		}
		setsid();
		if (fork() > 0) {
			_exit(0);
		}
	}

	rtc = open("/dev/rtc0", O_RDONLY);
	if (rtc < 0) {
		perror("Failed to open /dev/rtc0");
		return EXIT_FAILURE;
	}

	aw = alarmwakeup_open();

	run_daemon(rtc, aw);

	alarmwakeup_close(aw);

	return EXIT_FAILURE;
}
