PREFIX = /usr
BINDIR = $(PREFIX)/bin
LIBDIR = $(PREFIX)/lib
PKGCONFIGDIR = $(LIBDIR)/pkgconfig
INCLUDEDIR = $(PREFIX)/include
CFLAGS = -Wall -Wextra -Werror -Wno-unused-parameter -O2 -g
BINARIES = alarm-wakeup-daemon alarm-wakeup-ctl
INCLUDES = libalarmwakeup.h
SONAME = libalarmwakeup.so.0
LIBRARY = libalarmwakeup.so.0.2.0
TARGETS = $(BINARIES) $(LIBRARY) libalarmwakeup.pc

all: $(TARGETS)

alarm-wakeup-daemon: daemon.c libalarmwakeup.h $(LIBRARY)
	gcc $(CFLAGS) $< $(LIBRARY) -o $@

alarm-wakeup-ctl: ctl.c libalarmwakeup.h $(LIBRARY)
	gcc $(CFLAGS) $< $(LIBRARY) -o $@

$(LIBRARY): libalarmwakeup.c libalarmwakeup.h
	gcc $(CFLAGS) $< -o $@ -fPIC -shared -Wl,-soname,$(SONAME)

libalarmwakeup.pc: libalarmwakeup.pc.in
	@echo 'includedir=$(INCLUDEDIR)' > $@
	@echo 'libdir=$(LIBDIR)' >> $@
	@cat $< >> $@

.PHONY: clean
clean:
	rm -f $(TARGETS)

.PHONY: install
install:
	install -Dm755 $(BINARIES) -t $(DESTDIR)$(BINDIR)
	install -Dm644 $(LIBRARY) -t $(DESTDIR)$(LIBDIR)
	ln -sf $(LIBRARY) $(DESTDIR)$(LIBDIR)/$(SONAME)
	ln -sf $(LIBRARY) $(DESTDIR)$(LIBDIR)/libalarmwakeup.so
	install -Dm644 $(INCLUDES) -t $(DESTDIR)$(INCLUDEDIR)
	install -Dm644 libalarmwakeup.pc -t $(DESTDIR)$(PKGCONFIGDIR)
	install -Dm644 alarmwakeup.service -t $(DESTDIR)$(PREFIX)/share/superd/services
