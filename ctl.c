/*
 * Copyright (C) 2023 Affe Null
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libalarmwakeup.h"

static void usage(void)
{
	printf("Usage: alarm-wakeup-ctl [COMMAND] [ARGS...]\n");
	printf("Commands:\n");
	printf("  set  [HOUR]:[MINUTE] [LABEL] [CMD]    set an alarm\n");
	printf("  del  [HOUR]:[MINUTE] [LABEL] [CMD]    unset an alarm\n");
	printf("  list                                  list alarms\n");
	exit(1);
}

static void set_alarm(int argc, char * const *argv, bool set)
{
	struct alarmwakeup_file *aw = alarmwakeup_open();
	struct alarmwakeup_buf alarm = { 0 };

	if (argc < 5)
		usage();
	if (sscanf(argv[2], "%hhu:%hhu", &alarm.hour, &alarm.minute) != 2)
		usage();
	if (alarm.hour >= 24 || alarm.minute >= 60)
		usage();
	strncpy(alarm.label, argv[3], sizeof(alarm.label)-1);
	strncpy(alarm.command, argv[4], sizeof(alarm.command)-1);

	if (alarmwakeup_modify(aw, &alarm, set) < 0)
		perror("Failed to set alarm");
}

static void list_alarms(void)
{
	struct alarmwakeup_file *aw = alarmwakeup_open();
	struct alarmwakeup_buf alarm;

	while (alarmwakeup_read(aw, &alarm)) {
		printf("%02d:%02d  %-20s  %s\n",
		       alarm.hour, alarm.minute, alarm.label, alarm.command);
	}

	alarmwakeup_close(aw);
}

int main(int argc, char * const *argv)
{
	if (argc < 2)
		usage();
	if (0 == strcmp(argv[1], "set"))
		set_alarm(argc, argv, true);
	else if (0 == strcmp(argv[1], "del"))
		set_alarm(argc, argv, false);
	else if (0 == strcmp(argv[1], "list"))
		list_alarms();
	else
		usage();

	return 0;
}
