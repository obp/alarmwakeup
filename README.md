# alarmwakeup

The `alarm-wakeup-daemon` lets unprivileged users set alarms using a
system's real time clock (RTC). It is meant to be run individually by each
user, so in order for it to work, you should either install the daemon as
setuid root (in which case it will still run commands as the original user)
or give users direct access to the RTC.

An alarm has the following parameters:

 - `hour` (0-23), `minute` (0-59): time of day
 - `label`: arbitrary string to identify the purpose of the alarm
 - `command`: shell command to be executed when the alarm triggers

The RTC will wake up the system at the specified time and cause the command
to be executed by the daemon. The daemon will wait for the command to
terminate. While waiting, the system will be kept awake due to the use
of EPOLLWAKEUP in the daemon's event loop.

The alarms are stored in a database. The daemon reloads the database
whenever it is replaced with a `rename()` call. Writing to it directly
is not recommended.

A library is provided with a simple interface which allows setting, listing
and removing alarms (defined in libalarmwakeup.h, link with `-lalarmwakeup`):

- `alarmwakeup_open()` returns a newly allocated `struct alarmwakeup_file`
  structure, which represents the user's alarm database.
- `alarmwakeup_reopen(struct alarmwakeup_file *aw)` reloads the database
  and rewinds to the beginning of it.
- `alarmwakeup_read(struct alarmwakeup_file *aw, struct alarmwakeup_buf *alarm)`
  reads an alarm from the database into the specified buffer and returns 1,
  or returns 0 if no alarm could be read.
- `alarmwakeup_modify(struct alarmwakeup_file *aw, struct alarmwakeup_buf *alarm, bool set)`
  writes an alarm to the database if `set` is `true` or deletes an alarm if
  `set` is `false`. This process involves a call to alarmwakeup_reopen. It
  returns 1 if the operation was successful or sets errno and returns 0 if it
  failed.
- `alarmwakeup_close(struct alarmwakeup_file *aw)` closes the database and
  frees the `struct alarmwakeup_file`.

`alarm-wakeup-cli` provides a command-line interface to the alarm database.
