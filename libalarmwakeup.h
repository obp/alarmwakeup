/*
 * Copyright (C) 2023 Affe Null
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _LIBALARMWAKEUP_H_
#define _LIBALARMWAKEUP_H_

#include <stdbool.h>
#include <stdint.h>

struct alarmwakeup_buf {
	uint8_t hour;
	uint8_t minute;
	char label[20];
	char command[100];
};

struct alarmwakeup_file;

struct alarmwakeup_file *alarmwakeup_open(void);
void alarmwakeup_reopen(struct alarmwakeup_file *aw);
int alarmwakeup_read(struct alarmwakeup_file *aw, struct alarmwakeup_buf *alarm);
int alarmwakeup_modify(struct alarmwakeup_file *aw,
		       struct alarmwakeup_buf *alarm,
		       bool set);
void alarmwakeup_close(struct alarmwakeup_file *aw);

#endif
