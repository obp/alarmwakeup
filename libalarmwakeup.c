/*
 * Copyright (C) 2023 Affe Null
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <alloca.h>
#include <errno.h>
#include <fcntl.h>
#include <pwd.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "libalarmwakeup.h"

struct alarmwakeup_file {
	int file;
};

void alarmwakeup_reopen(struct alarmwakeup_file *aw)
{
	char *home = getpwuid(getuid())->pw_dir;
	char *path = alloca(strlen(home) + strlen("/.config/alarmwakeup") + 1);

	if (aw->file >= 0)
		close(aw->file);

	sprintf(path, "%s/.config/alarmwakeup", home);
	aw->file = open(path, O_RDONLY|O_CLOEXEC);
}

struct alarmwakeup_file *alarmwakeup_open(void)
{
	struct alarmwakeup_file *aw = malloc(sizeof(struct alarmwakeup_file));

	aw->file = -1;
	alarmwakeup_reopen(aw);

	return aw;
}

int alarmwakeup_read(struct alarmwakeup_file *aw, struct alarmwakeup_buf *alarm)
{
	int ret;

	if (aw->file < 0)
		return 0;

	ret = read(aw->file, alarm, sizeof(struct alarmwakeup_buf));
	if (ret < 0)
		return 0;
	if (ret != sizeof(struct alarmwakeup_buf)) {
		errno = EILSEQ;
		return 0;
	}

	return 1;
}

static int put_alarm(int fd, struct alarmwakeup_buf *alarm)
{
	int ret;
	ret = write(fd, alarm, sizeof(struct alarmwakeup_buf));
	if (ret < 0)
		return -1;
	if (ret != sizeof(struct alarmwakeup_buf)) {
		errno = EAGAIN;
		return -1;
	}
	return 0;
}

static bool is_same_alarm(struct alarmwakeup_buf *a, struct alarmwakeup_buf *b)
{
	return a->hour == b->hour && a->minute == b->minute &&
		0 == strcmp(a->label, b->label) &&
		0 == strcmp(a->command, b->command);
}

static int compare_alarm(struct alarmwakeup_buf *a, struct alarmwakeup_buf *b)
{
	return (a->hour * 60 + a->minute) - (b->hour * 60 + b->minute);
}

int alarmwakeup_modify(struct alarmwakeup_file *aw,
		       struct alarmwakeup_buf *alarm,
		       bool set)
{
	struct alarmwakeup_buf tmp;
	char *home = getpwuid(getuid())->pw_dir;
	char *npath = alloca(strlen(home) + strlen("/.config/alarmwakeup.new") + 1);
	char *path = alloca(strlen(home) + strlen("/.config/alarmwakeup") + 1);
	int i, wfile = -1;
	bool done = false;

	alarmwakeup_reopen(aw);

	sprintf(npath, "%s/.config/alarmwakeup.new", home);
	for (i = 0; i < 20; i++) {
		wfile = open(npath, O_CREAT|O_WRONLY|O_TRUNC|O_EXCL, 0600);
		if (wfile < 0)
			usleep(100000);
		else
			break;
	}

	if (wfile < 0) {
		fprintf(stderr, "libalarmwakeup: suspected stale alarmwakeup.new file, overwriting\n");
		wfile = open(npath, O_CREAT|O_WRONLY|O_TRUNC, 0600);
		if (wfile < 0)
			return -1;
	}

	while (alarmwakeup_read(aw, &tmp)) {
		/* skip alarm if it needs to be deleted */
		if (!set && !done && is_same_alarm(alarm, &tmp)) {
			done = true;
			continue;
		}
		/* if inserting, write it at the right position */
		if (set && !done && compare_alarm(alarm, &tmp) < 0) {
			if (put_alarm(wfile, alarm) < 0)
				return -1;
			done = true;
		}
		if (put_alarm(wfile, &tmp) < 0)
			return -1;
	}

	/* insert alarm at the end if no position found */
	if (set && !done) {
		if (put_alarm(wfile, alarm) < 0)
			return -1;
	}

	close(wfile);

	sprintf(path, "%s/.config/alarmwakeup", home);
	return rename(npath, path);
}

void alarmwakeup_close(struct alarmwakeup_file *aw)
{
	if (aw->file)
		close(aw->file);

	free(aw);
}
